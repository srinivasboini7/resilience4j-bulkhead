package com.srini.bulkhead.service;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * The type App service.
 */
@Service
@Slf4j
public class AppService {

    private final AtomicInteger counter = new AtomicInteger() ;

    /**
     * Get.
     */
    @Bulkhead(name = "serviceA", type = Bulkhead.Type.SEMAPHORE, fallbackMethod = "fallbackA")
    public void get(){
        log.info("get method called {} time(s)", counter.incrementAndGet());
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Fallback a.
     *
     * @param throwable the throwable
     */
    public void fallbackA(Throwable throwable){
        log.error("exception recorded {}", throwable);
        log.info("fallback called ");
    }


}
